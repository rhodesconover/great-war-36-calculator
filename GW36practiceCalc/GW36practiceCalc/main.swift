//
//  main.swift
//  GW36practiceCalc
//
//  Created by Cade Dotson on 12/26/20.
//

import Foundation

var attacker:Army = .init(Attack: true)
var defender:Army = .init(Attack: false)



//print( "Launching Battle Calculator!" )


attacker.addUnit( Unit: MEDIUM_ARMOR )
attacker.addUnit( Unit: MEDIUM_ARMOR )
attacker.addUnit( Unit: MECHANIZED_INFANTRY )
attacker.addUnit( Unit: MECHANIZED_INFANTRY )

defender.addUnit( Unit: INFANTRY )
defender.addUnit( Unit: INFANTRY )
defender.addUnit( Unit: MILITIA )
defender.addUnit( Unit: MILITIA )

attacker.printMe()
defender.printMe()



print(attacker.hitCount)
print(defender.hitCount)


var calc:Calc = .init(AttackingArmy: attacker, DefendingArmy: defender, Terrain: FORTIFICATION)
calc.runCalc()


