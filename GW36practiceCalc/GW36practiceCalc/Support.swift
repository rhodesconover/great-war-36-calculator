
import Foundation


//************************************************************
// Function Name: damage_ships( unit_type )
//
// Details: This function removes a unit of the given type from
//      from the army, it also updates Army.unitCount
//
//
//************************************************************
func damage_ships( Army army: Army, UnitType unitType: Int, Hits hits: Int ) -> Int
    {
    var hits_applied = 0
    var local_hits = hits
    
    if( army.Units[ unitType ].count > 0 )
        {
        for index in 0..<army.Units[ unitType ].count
            {
            while ( ( army.Units[ unitType ][ index ].hits > 1 ) && ( local_hits > 0 ) )
                {
                army.remUnit(Unit: unitType )
                hits_applied += 1
                local_hits -= 1
                }
            }
        }
    return hits_applied
    } /* damage_ships */

func remove_first_hit ( Army army: Army, UnitType unitType: Int, Hits hits: Int) -> Int
    {
    var hits_applied = 0
    var local_hits = hits
    if( army.Units[ unitType ].count > 0)
        {
        for index in 0..<army.Units[ unitType ].count
            {
            if ( ( army.Units[ unitType ][ index ].hits == 3 ) && ( local_hits > 0 ) )
                {
                print("removing first hit from heavy battleship")
                army.remUnit(Unit: unitType )
                hits_applied += 1
                local_hits -= 1
                }
            }
        }
    return hits_applied
    }

func remove_second_hit ( Army army: Army, UnitType unitType: Int, Hits hits: Int) -> Int
    {
    var hits_applied = 0
    var local_hits = hits
    if( army.Units[ unitType ].count > 0)
        {
        for index in 0..<army.Units[ unitType ].count
            {
            if ( ( army.Units[ unitType ][ index ].hits == 2 ) && ( local_hits > 0 ) )
                {
                print("removing second hit from heavy battleship")
                army.remUnit(Unit: unitType )
                hits_applied += 1
                local_hits -= 1
                }
            }
        }
    return hits_applied
    }
