//
//  Unit.swift
//  GW36practiceCalc
//
//  Created by Cade Dotson on 12/26/20.
//

import Foundation

struct Unit {
    var title: String
    var name:Int
    var attack:Int
    var defense:Int
    var bombard:Int
    var cost:Int
    var hits:Int
    var unitClass:String

    //************************************************************
    // Function Names: Unit.init()
    //
    // Details: This constructor function for a unit just sets
    //      the data values for the unit based on the name of
    //      the unit.
    //
    //************************************************************
    init (Name name:Int)
        {
        self.name = name
        
        self.title = UNIVERSAL_UNIT_DATA[ name ][ UUSI ] as! String
        self.attack = UNIVERSAL_UNIT_DATA[ name ][ UUAI ] as! Int
        self.defense = UNIVERSAL_UNIT_DATA[ name ][ UUDI ] as! Int
        self.bombard = UNIVERSAL_UNIT_DATA[ name ][ UUBI ] as! Int
        self.cost = UNIVERSAL_UNIT_DATA[ name ][ UUCoI ] as! Int
        self.hits = UNIVERSAL_UNIT_DATA[ name ][ UUHI ] as! Int
        self.unitClass = UNIVERSAL_UNIT_DATA[ name ][ UUCI ] as! String
        }
} /* Class Unit */
