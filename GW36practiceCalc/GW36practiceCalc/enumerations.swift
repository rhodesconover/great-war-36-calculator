import Foundation


let UNITED_STATES_MARINE_CORPS = 0
let INFANTRY = 1
let MILITIA = 2
let MOUNTAIN_INFANTRY = 3
let MARINE_INFANTRY = 4
let AIRBORNE_INFANTRY = 5
let COLONIAL_INFANTRY = 6
let CAVALRY = 7
let MOTORIZED_INFANTRY = 8
let MECHANIZED_INFANTRY = 9
let ARTILLERY = 10
let SELF_PROPELLED_ARTILLERY = 11
let ANTI_AIRCRAFT_ARTILLERY = 12
let TANK_DESTROYER = 13
let LIGHT_ARMOR = 14
let MEDIUM_ARMOR = 15
let FIGHTER = 16
let TACTICAL_BOMBER = 17
let MEDIUM_BOMBER = 18
let STRATEGIC_BOMBER = 19
let AIR_TRANSPORT = 20
let SEAPLANE = 21
let TORPEDO_BOAT_DESTROYER = 22
let DESTROYER = 23
let LIGHT_CRUISER = 24
let HEAVY_CRUISER = 25
let COASTAL_DEFENSE_SHIP = 26
let BATTLECRUISER = 27
let BATTLESHIP = 28
let FAST_BATTLESHIP = 29
let LIGHT_CARRIER = 30
let FLEET_CARRIER = 31
let COASTAL_SUBMARINE = 32
let SUBMARINE = 33
let NAVAL_TRANSPORT = 34
let ADVANCED_ARTILLERY = 35
let ADVANCED_SELF_PROPELLED_ARTILLERY = 36
let ADVANCED_MECHANIZED_INFANTRY = 37
let HEAVY_ARMOR = 38
let JET_FIGHTER = 39
let HEAVY_STRATEGIC_BOMBER = 40
let HEAVY_BATTLESHIP = 41
let HEAVY_CARRIER = 42
let ADVANCED_SUBMARINE = 43
let ATTACK_TRANSPORT = 44
let GURKHA = 45
let FRENCH_FOREIGN_LEGION = 46
let SS_PANZERGRENADIERS =  47
let TIGER_1E = 48
let SPECIAL_NAVAL_LANDING_FORCE = 49
let FULL_DESTROYER = 50
let KATYUSHA = 51
let DAMAGE_BATTLESHIPS = 52
let DAMAGE_CARRIERS = 53
let DAMAGE_FIRST_HEAVY_BATTLESHIP = 54
let DAMAGE_SECOND_HEAVY_BATTLESHIP = 55

// make sure to update this.
let UNIT_TYPE_COUNT = 56


let UUSI = 1    // Universal Unit String Index
let UUAI = 2    // Universal Unit Attack Index
let UUDI = 3    // Universal Unit Defense Index
let UUBI = 4    // Universal Unit Bombard Index
let UUCoI = 5    // Universal Unit Cost Index
let UUHI = 6    // Universal Unit Hits Index
let UUCI = 7    // Universal Unit Class Index


let UNIVERSAL_UNIT_DATA = [
//     ID   STRING_NAME                     ATTACK   DEFENSE  BOMBARD   COST    HITS    CLASS
    [  0,   "UNITED_STATES_MARINE_CORPS",   3,       5,       0,        5,      1,      "INFANTRY"],
    [  1,   "INFANTRY",                     2,       4,       0,        3,      1,      "INFANTRY"],
    [  2,   "MILITIA",                      1,       2,       0,        2,      1,      "INFANTRY"],
    [  3,   "MOUNTAIN_INFANTRY",            2,       4,       0,        4,      1,      "INFANTRY"],
    [  4,   "MARINE_INFANTRY",              2,       4,       0,        4,      1,      "INFANTRY"],
    [  5,   "AIRBORNE_INFANTRY",            2,       4,       0,        4,      1,      "INFANTRY"],
    [  6,   "COLONIAL_INFANTRY",            2,       4,       0,        4,      1,      "INFANTRY"],
    [  7,   "CAVALRY",                      3,       2,       0,        3,      1,      "VEHICLE"],
    [  8,   "MOTORIZED_INFANTRY",           2,       4,       0,        4,      1,      "VEHICLE"],
    [  9,   "MECHANIZED_INFANTRY",          3,       4,       0,        4,      1,      "VEHICLE"],
    [  10,  "ARTILLERY",                    3,       3,       0,        4,      1,      "ARTILLERY"],
    [  11,  "SELF_PROPELLED_INFANTRY",      3,       3,       0,        5,      1,      "ARTILLERY"],
    [  12,  "ANTI_AIRCRAFT_ARTILLERY",      3,       3,       0,        4,      1,      "ARTILLERY"],
    [  13,  "TANK_DESTROYER",               3,       4,       0,        5,      1,      "VEHICLE"],
    [  14,  "LIGHT_ARMOR",                  4,       3,       0,        5,      1,      "VEHICLE"],
    [  15,  "MEDIUM_ARMOR",                 6,       5,       0,        6,      1,      "VEHICLE"],
    [  16,  "FIGHTER",                      6,       6,       0,        10,     1,      "AIR"],
    [  17,  "TACTICAL_BOMBER",              7,       5,       0,        11,     1,      "AIR"],
    [  18,  "MEDIUM_BOMBER",                7,       4,       0,        11,     1,      "AIR"],
    [  19,  "STRATEGIC_BOMBER",             2,       2,       0,        12,     1,      "AIR"],
    [  20,  "AIR_TRANSPORT",                0,       0,       0,        8,      1,      "AIR"],
    [  21,  "SEAPLANE",                     3,       1,       0,        7,      1,      "AIR"],
    [  22,  "TORPEDO_BOAT_DESTROYER",       2,       2,       0,        0,      1,      "SHIP"],
    [  23,  "DESTROYER",                    4,       4,       0,        7,      1,      "SHIP"],
    [  24,  "LIGHT_CRUISER",                5,       5,       1,        8,      1,      "SHIP"],
//     ID   STRING_NAME                     ATTACK   DEFENSE  BOMBARD   COST    HITS    CLASS
    [  25,  "HEAVY_CRIUSER",                6,       6,       2,        10,     1,      "SHIP"],
    [  26,  "COASTAL_DEFENSE_SHIP",         6,       6,       3,        0,      1,      "SHIP"],
    [  27,  "BATTLECRUISER",                7,       7,       3,        12,     1,      "SHIP"],
    [  28,  "BATTLESHIP",                   8,       8,       4,        15,     2,      "SHIP"],
    [  29,  "FAST_BATTLESHIP",              8,       8,       4,        18,     2,      "SHIP"],
    [  30,  "LIGHT_CARRIER",                0,       1,       0,        8,      1,      "SHIP"],
    [  31,  "FLEET_CARRIER",                0,       2,       0,        15,     2,      "SHIP"],
    [  32,  "COASTAL_SUBMARINE",            2,       2,       0,        0,      1,      "SHIP"],
    [  33,  "SUBMARINE",                    3,       3,       0,        6,      1,      "SHIP"],
    [  34,  "NAVAL_TRANSPORT",              0,       0,       0,        6,      1,      "SHIP"],
    [  35,  "ADVANCED_ARTILLERY",           4,       4,       0,        4,      1,      "ARTILLERY"],
    [  36,  "ADVANCED_SELF_PROPELLED_ARTILLERY", 4,  4,       0,        5,      1,      "ARTILLERY"],
    [  37,  "ADVANCED_MECHANIZED_INFANTRY", 4,       5,       0,        4,      1,      "VEHICLE"],
    [  38,  "HEAVY_ARMOR",                  8,       7,       0,        8,      1,      "VEHICLE"],
    [  39,  "JET_FIGHTER",                  8,       8,       0,        12,     1,      "AIR"],
    [  40,  "HEAVY_STRATEGIC_BOMBER",       2,       3,       0,        13,     1,      "AIR"],
    [  41,  "HEAVY_BATTLESHIP",             10,      10,      5,        21,     3,      "SHIP"],
    [  42,  "HEAVY_CARRIER",                0,       2,       0,        18,     2,      "SHIP"],
    [  43,  "ADVANCED_SUBMARINE",           4,       4,       0,        7,      1,      "SHIP"],
    [  44,  "ATTACK_TRANSPORT",             0,       0,       0,        9,      1,      "SHIP"],
    [  45,  "GURKHA",                       2,       4,       0,        4,      1,      "INFANTRY"],
    [  46,  "FRENCH_FOREIGN_LEGION",        2,       4,       0,        4,      1,      "INFANTRY"],
    [  47,  "SS_PANZERGRENADIERS",          4,       5,       0,        4,      1,      "VEHICLE"],
    [  48,  "TIGER_1E",                     8,       7,       0,        8,      1,      "VEHICLE"],
    [  49,  "SPECIAL_NAVAL_LANDING_FORCE",  3,       5,       0,        5,      1,      "INFANTRY"],
    [  50,  "FULL_DESTROYER",               3,       3,       0,        7,      1,      "SHIP"],
    [  51,  "KATYUSHA",                     5,       4,       0,        5,      1,      "ARTILLERY"]
    ]
       


// Disciplines of removal of units:
let MUST_TAKE_TERRITORY_TACTIC = 0
let DEFENSE_TACTIC = 1
let DESTROY_ALL_ENEMIES = 2
let CUSTOM = 3


let MUST_TAKE_TERRITORY =
    [
    DAMAGE_FIRST_HEAVY_BATTLESHIP,
    DAMAGE_CARRIERS,
    MILITIA,
    INFANTRY,
    AIRBORNE_INFANTRY,
    CAVALRY,
    COLONIAL_INFANTRY,
    MOTORIZED_INFANTRY,
    MARINE_INFANTRY,
    MOUNTAIN_INFANTRY,
    GURKHA,
    FRENCH_FOREIGN_LEGION,
    ANTI_AIRCRAFT_ARTILLERY,
    ARTILLERY,
    MECHANIZED_INFANTRY,
    SELF_PROPELLED_ARTILLERY,
    LIGHT_ARMOR,
    UNITED_STATES_MARINE_CORPS,
    ADVANCED_ARTILLERY,
    ADVANCED_MECHANIZED_INFANTRY,
    SS_PANZERGRENADIERS,
    ADVANCED_SELF_PROPELLED_ARTILLERY,
    TANK_DESTROYER,
    KATYUSHA,
    MEDIUM_ARMOR,
    HEAVY_ARMOR,
    TIGER_1E,
    TORPEDO_BOAT_DESTROYER,
    COASTAL_SUBMARINE,
    SUBMARINE,
    SEAPLANE,
    DESTROYER,
    DAMAGE_SECOND_HEAVY_BATTLESHIP,
    DAMAGE_BATTLESHIPS,
    ADVANCED_SUBMARINE,
    AIR_TRANSPORT,
    LIGHT_CRUISER,
    COASTAL_DEFENSE_SHIP,
    AIR_TRANSPORT,
    LIGHT_CRUISER,
    COASTAL_DEFENSE_SHIP,
    HEAVY_CRUISER,
    LIGHT_CARRIER,
    FLEET_CARRIER,
    HEAVY_CARRIER,
    FIGHTER,
    BATTLECRUISER,
    MEDIUM_BOMBER,
    TACTICAL_BOMBER,
    STRATEGIC_BOMBER,
    JET_FIGHTER,
    FULL_DESTROYER,
    BATTLESHIP,
    FAST_BATTLESHIP,
    HEAVY_BATTLESHIP,
    NAVAL_TRANSPORT,
    ATTACK_TRANSPORT
    ]



let NO_TERRAIN          = 0
let MOUNTAIN_TERRITORY  = 1
let MOUNTAIN_BORDER     = 2
let DESERT_TERRITORY    = 3
let DESERT_BORDER       = 4
let JUNGLE_TERRITORY    = 5
let JUNGLE_BORDER       = 6
let MARSH_TERRITORY     = 7
let MARSH_BORDER        = 8
let RIVER               = 9
let CITY                = 10
let CITY_SURROUNDED     = 11
let FORTIFICATION       = 12
let MAGINOT_LINE        = 13
let AIRBORNE_ASSAULT    = 14


let UTSI = 1 //Universal Terrain String Index
let UTDI = 2 //Universal Terrain Duration Index

let TERRAIN_DATA = [
//  INDEX   NAME                   DURATION(-1 indicates all rounds)
    [NO_TERRAIN,            "NO_TERRAIN",            -1],
    [MOUNTAIN_TERRITORY,    "MOUNTAIN_TERRITORY",    -1],
    [MOUNTAIN_BORDER,       "MOUNTAIN_BORDER",        1],
    [DESERT_TERRITORY,      "DESERT_TERRITORY",      -1],
    [DESERT_BORDER,         "DESERT_BORDER",          1],
    [JUNGLE_TERRITORY,      "JUNGLE_TERRITORY",      -1],
    [JUNGLE_BORDER,         "JUNGLE_BORDER",          1],
    [MARSH_TERRITORY,       "MARSH_TERRITORY",       -1],
    [MARSH_BORDER,          "MARSH_BORDER",           1],
    [RIVER,                 "RIVER",                  1],
    [CITY,                  "CITY",                  -1],
    [CITY_SURROUNDED,       "CITY_SURROUNDED",       -1],
    [FORTIFICATION,         "FORTIFICATION",          1],
    [MAGINOT_LINE,          "MAGINOT_LINE",           1],
    [AIRBORNE_ASSAULT,      "AIRBORNE_ASSAULT",       1]
]
