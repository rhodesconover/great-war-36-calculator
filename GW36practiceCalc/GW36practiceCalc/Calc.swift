//
//  Calc.swift
//  GW36practiceCalc
//
//  Created by Cade Dotson on 12/28/20.
//

import Foundation

let NUMB_RUNS = 1000

class Calc {
    var attacker:Army
    var defender:Army
    var attackerWins:Float = 0
    var defenderWins:Float = 0
    var tieCount:Float = 0
    var attackerCost:Float = 0
    var defenderCost:Float = 0
    var attackUnitCount:Int
    var defendUnitCount:Int
    var terrain:Int
    var terrainDuration:Int
    var vehicleRetreat:Bool
    
    var attacker_casualties_tactic:Int = MUST_TAKE_TERRITORY_TACTIC
    var defender_casualties_tactic:Int = MUST_TAKE_TERRITORY_TACTIC
    
    //************************************************************
    // Function Name: Constructor for Calc Class
    //
    // Details: This function is used to construct the Calc Object.
    //      It takes an attacking army and defending army and
    //      populates some elements of the class but not all?
    //      TODO: Why does this not initialize all values in
    //          the class?
    //          ------Doesn't need to initialize all because the rest always start at 0 -Cade
    //
    //************************************************************
    init(AttackingArmy attacker:Army, DefendingArmy defender:Army, Terrain terrain:Int)
        {
        self.attacker = attacker
        self.defender = defender
        attackUnitCount = attacker.getUnitCount()
        defendUnitCount = defender.getUnitCount()
        self.terrain = terrain
        self.terrainDuration = TERRAIN_DATA[terrain][UTDI] as! Int
        self.vehicleRetreat = false
        
        if( ( self.terrain == DESERT_BORDER ) ||
            ( self.terrain == DESERT_TERRITORY ) )
            {
            vehicleRetreat = true
            }
        } /* Calc.init() */
    
    
    //************************************************************
    // Function Name: Calc.roll()
    //
    // Details: This function takes a list of unit objects of a
    //      specific type and determines how many of these units
    //      "hit" using a random value generator. It returns 1.)
    //      how many units hit and 2.) how many of this class
    //      have to retreat.
    //
    //************************************************************
    func roll(Unit unit:[Unit], Attack attack:Bool, Terrain terr:Int ) -> ( Int, Int )
        {
        var count = 0
        var ret_units = 0
        if unit.count > 0
            {
            for i in 0...unit.count-1
                {
                let number = Int.random(in: 1...12)
                if attack
                    {
                    print("Attacker attack value: \(unit[i].attack)")
                    if number <= unit[i].attack
                        {
                        count += 1
                        }
                    if( number == 12 ) && ( vehicleRetreat )
                        {
                        ret_units += 1
                        }
                    }
                else
                    {
                    print("Defender defense value: \(unit[i].defense)")
                    if number <= unit[i].defense
                        {
                        count += 1
                        }
                    }
                }
            return ( count, ret_units )
            }
        else
            {
            return ( 0, 0 )
            }
        } /* Calc.roll() */
    
    
    //************************************************************
    // Function Name: Calc.rollFirstStrike()
    //
    // Details: This function takes a list of unit objects and an
    //          attack value and rolls for hits based on the
    //          given attack
    //
    //
    //************************************************************
    func rollFirstStrike(Army army:Army, Opposing_Army opp_army:Army, Attack attack:Bool ) -> Int
        {
        var local_hits = 0
        if !attack
            {
            if self.terrain == FORTIFICATION
                {
                var tempUnits:[Unit] = []
                let tempUnit:Unit = .init(Name: KATYUSHA)
                tempUnits.append(tempUnit)
                tempUnits.append(tempUnit)
                local_hits += roll(Unit: tempUnits, Attack: true, Terrain: self.terrain ).0
                }
            else if self.terrain == MAGINOT_LINE
                {
                var tempUnits:[Unit] = []
                let tempUnit:Unit = .init(Name: MEDIUM_ARMOR)
                tempUnits.append(tempUnit)
                tempUnits.append(tempUnit)
                local_hits += roll(Unit: tempUnits, Attack: true, Terrain: self.terrain ).0
                }
            /*-----------------------------------------
            The above code is a bit misleading, these
             represent first strike hits from fortifications,
             the KATYUSHA and MEDIUM_ARMOR simply have
             equivalent attack values so I used them in place
             of creating new Unit objects
            -----------------------------------------*/
            if army.Units[ARTILLERY].count > 0
                {
                local_hits += roll(Unit: army.Units[ARTILLERY], Attack: false, Terrain: self.terrain ).0
                }
            if army.Units[SELF_PROPELLED_ARTILLERY].count > 0
                {
                local_hits += roll(Unit: army.Units[SELF_PROPELLED_ARTILLERY], Attack: false, Terrain: self.terrain ).0
                }
            if army.Units[ADVANCED_ARTILLERY].count > 0
                {
                local_hits += roll(Unit: army.Units[ADVANCED_ARTILLERY], Attack: false, Terrain: self.terrain ).0
                }
            /*-----------------------------------------
            Subs only get first strike if there are
             no destroyers present
            -----------------------------------------*/
            if opp_army.Units[DESTROYER].count == 0
                {
                if army.Units[COASTAL_SUBMARINE].count > 0
                    {
                    local_hits += roll(Unit: army.Units[COASTAL_SUBMARINE], Attack: false, Terrain: self.terrain ).0
                    }
                if army.Units[SUBMARINE].count > 0
                    {
                    local_hits += roll(Unit: army.Units[SUBMARINE], Attack: false, Terrain: self.terrain ).0
                    }
                if army.Units[ADVANCED_SUBMARINE].count > 0
                    {
                    local_hits += roll(Unit: army.Units[ADVANCED_SUBMARINE], Attack: false, Terrain: self.terrain ).0
                    }
                }
            }
        else if attack
            {
            if army.Units[ARTILLERY].count > 0
                {
                local_hits += roll(Unit: army.Units[ARTILLERY], Attack: true, Terrain: self.terrain ).0
                }
            if army.Units[SELF_PROPELLED_ARTILLERY].count > 0
                {
                local_hits += roll(Unit: army.Units[SELF_PROPELLED_ARTILLERY], Attack: true, Terrain: self.terrain ).0
                }
            if army.Units[ADVANCED_ARTILLERY].count > 0
                {
                local_hits += roll(Unit: army.Units[ADVANCED_ARTILLERY], Attack: true, Terrain: self.terrain ).0
                }
            if opp_army.Units[DESTROYER].count == 0
                {
                if army.Units[COASTAL_SUBMARINE].count > 0
                    {
                    local_hits += roll(Unit: army.Units[COASTAL_SUBMARINE], Attack: true, Terrain: self.terrain ).0
                    }
                if army.Units[SUBMARINE].count > 0
                    {
                    local_hits += roll(Unit: army.Units[SUBMARINE], Attack: true, Terrain: self.terrain ).0
                    }
                if army.Units[ADVANCED_SUBMARINE].count > 0
                    {
                    local_hits += roll(Unit: army.Units[ADVANCED_SUBMARINE], Attack: true, Terrain: self.terrain ).0
                    }
                }
            }
        return local_hits
        }
    
    //************************************************************
    // Function Name: Calc.countTotalHits()
    //
    // Details: This function takes an army, and then determines
    //      how many hits the army as a whole generated. It takes
    //      an army object and returns an integer for how many
    //      total hits that army generated.
    //
    //************************************************************
    func countTotalHits(Army army:Army, Opposing_Army opp_army:Army, Round round:Int ) -> Int
        {
        //print("counting hits...")
        var count = 0
        var index = 0
        for type in army.Units
            {
            if (round == 0 && index == ARTILLERY)
                {
                }
            else if (round == 0 && index == SELF_PROPELLED_ARTILLERY)
                {
                }
            else if (round == 0 && index == ADVANCED_ARTILLERY)
                {
                }
            else if (round == 0 && index == COASTAL_SUBMARINE)
                {
                if opp_army.Units[DESTROYER].count > 0
                    {
                    count += roll(Unit: type, Attack: army.attack, Terrain: self.terrain ).0
                    }
                }
            else if (round == 0 && index == SUBMARINE)
                {
                if opp_army.Units[DESTROYER].count > 0
                    {
                    count += roll(Unit: type, Attack: army.attack, Terrain: self.terrain).0
                    }
                }
            else if (round == 0 && index == ADVANCED_SUBMARINE)
                {
                if opp_army.Units[DESTROYER].count > 0
                    {
                    count += roll(Unit: type, Attack: army.attack, Terrain: self.terrain ).0
                    }
                }
            else
                {
                let result = roll(Unit: type, Attack: army.attack, Terrain: self.terrain )
                count += result.0
                let numb_retreats = result.1
                if( numb_retreats > 0 )
                    {
                    print( "Retreating ", numb_retreats, " units." )
                    for _ in 0..<numb_retreats
                        {
                        army.retreatUnit(Unit: type[0].name )
                        }
                    }
                }
            index += 1
            }
        //print("\(count) hits")
        return  count
    } /* Calc.countTotalHits() */
    
    
    
    //************************************************************
    // Function Name: Calc.removeCasualties()
    //
    // Details: This function removes casualities from the army
    //      object provided. It takes an army object and a number
    //      of hits and removes units from the army object. It
    //      returns a new army object upon completion.
    //
    //      TODO: Why not pass Army by reference to remove the
    //          return statement and reduce memory bloat
    //      TODO: We will need to add various options for what
    //          order of units to kill given different player
    //          objectives.
    //
    //************************************************************
    func removeCasualties(Army army:Army, numHits hits:Int) -> (Army) {
        var local_hits = hits
        print("removeCasualties has received \(local_hits) hits")
        var unit_type: Int = 0
        
        while local_hits > 0
            {
            if( attacker_casualties_tactic == MUST_TAKE_TERRITORY_TACTIC )
                {
                for i in 0...( UNIT_TYPE_COUNT - 1 )
                    {
                    unit_type = MUST_TAKE_TERRITORY[ i ]
                    if( unit_type == DAMAGE_BATTLESHIPS )
                        {
                        local_hits -= damage_ships(Army: army, UnitType: BATTLESHIP, Hits: local_hits )
                        local_hits -= damage_ships(Army: army, UnitType: FAST_BATTLESHIP, Hits: local_hits)
                        continue
                        }
                    else if( unit_type == DAMAGE_CARRIERS )
                        {
                        local_hits -= damage_ships(Army: army, UnitType: FLEET_CARRIER, Hits: local_hits )
                        local_hits -= damage_ships(Army: army, UnitType: HEAVY_CARRIER, Hits: local_hits )
                        continue
                        }
                    else if( unit_type == DAMAGE_FIRST_HEAVY_BATTLESHIP )
                        {
                        local_hits -= remove_first_hit(Army: army, UnitType: HEAVY_BATTLESHIP, Hits: local_hits )
                        continue
                        }
                    else if( unit_type == DAMAGE_SECOND_HEAVY_BATTLESHIP )
                        {
                        local_hits -= remove_second_hit(Army: army, UnitType: HEAVY_BATTLESHIP, Hits: local_hits )
                        continue
                        }
                    while (army.Units[unit_type].count > 0 && local_hits > 0)
                        {
                        print("Removing \(local_hits) hits...")
                        army.remUnit(Unit: unit_type)
                        local_hits -= 1
                        }
                    }
                }
            }
        return (army)
        } /* Calc.removeCasualities() */
    
    //************************************************************
    // Function Name: Calc.adjustArmyForTerrain()
    //
    // Details: This function edits combat values for units within
    //  a given army based on the Calc.terrain modifier
    //
    //************************************************************
    func adjustArmyForTerrain(Army army:Army)
        {
        if self.terrain == MOUNTAIN_TERRITORY || self.terrain == MOUNTAIN_BORDER
            {
            army.changeClassValue(Value: -1, UnitClass: "INFANTRY", Attack: true)
            army.changeClassValue(Value: -1, UnitClass: "ARTILLERY", Attack: true)
            army.changeClassValue(Value: -1, UnitClass: "VEHICLE", Attack: true)
            army.changeUnitValue(Value: 1, Unit: MOUNTAIN_INFANTRY, Attack: true)
            army.changeUnitValue(Value: 1, Unit: GURKHA, Attack: true)
            if self.terrain == MOUNTAIN_TERRITORY
                {
                army.changeUnitValue(Value: 1, Unit: MOUNTAIN_INFANTRY, Attack: false)
                army.changeUnitValue(Value: 1, Unit: GURKHA, Attack: false)
                }
            }
        else if self.terrain == DESERT_TERRITORY || self.terrain == DESERT_BORDER
            {
            army.changeClassValue(Value: -1, UnitClass: "INFANTRY", Attack: true)
            army.changeClassValue(Value: -1, UnitClass: "ARTILLERY", Attack: true)
            army.changeClassValue(Value: -1, UnitClass: "VEHICLE", Attack: true)
            army.changeUnitValue(Value: 1, Unit: FRENCH_FOREIGN_LEGION, Attack: true)
            //TO DO: Must utilize Army.retreatUnit() to make VEHICLE class units retreat on a roll of 12
            }
        else if self.terrain == JUNGLE_TERRITORY || self.terrain == JUNGLE_BORDER
            {
            army.changeClassValue(Value: -2, UnitClass: "VEHICLE", Attack: true)
            army.changeClassValue(Value: -2, UnitClass: "VEHICLE", Attack: false)
            //TO DO: JUNGLES also affect STRATEGIC_BOMBERS during carpet bombing by reducing number of dice rolled
            }
        else if self.terrain == MARSH_TERRITORY || self.terrain == MARSH_BORDER
            {
            army.changeClassValue(Value: -2, UnitClass: "VEHICLE", Attack: true)
            army.changeClassValue(Value: -2, UnitClass: "VEHICLE", Attack: false)
            }
        else if self.terrain == RIVER
            {
            army.changeClassValue(Value: -1, UnitClass: "INFANTRY", Attack: true)
            army.changeClassValue(Value: -1, UnitClass: "VEHICLE", Attack: true)
            army.changeUnitValue(Value: 1, Unit: MARINE_INFANTRY, Attack: true)
            army.changeUnitValue(Value: 1, Unit: SPECIAL_NAVAL_LANDING_FORCE, Attack: true)
            army.changeUnitValue(Value: 1, Unit: UNITED_STATES_MARINE_CORPS, Attack: true)
            }
        else if self.terrain == CITY
            {
            army.changeClassValue(Value: 1, UnitClass: "INFANTRY", Attack: false)
            //TO DO: INFANTRY class units target select of 1 against VEHICLE class units
            }
        else if self.terrain == CITY_SURROUNDED
            {
            army.changeClassValue(Value: -1, UnitClass: "INFANTRY", Attack: false)
            army.changeClassValue(Value: -1, UnitClass: "ARTILLERY", Attack: false)
            army.changeClassValue(Value: -1, UnitClass: "VEHICLE", Attack: false)
            }
        else if self.terrain == FORTIFICATION
            {
            army.changeClassValue(Value: 2, UnitClass: "INFANTRY", Attack: false)
            army.changeClassValue(Value: 2, UnitClass: "ARTILLERY", Attack: false)
            army.changeClassValue(Value: 2, UnitClass: "VEHICLE", Attack: false)
            //TO DO: Fortifications provide two FIRST STRIKE shots at 5
            }
        else if self.terrain == MAGINOT_LINE
            {
            army.changeClassValue(Value: 3, UnitClass: "INFANTRY", Attack: false)
            army.changeClassValue(Value: 3, UnitClass: "ARTILLERY", Attack: false)
            army.changeClassValue(Value: 3, UnitClass: "VEHICLE", Attack: false)
            //TO DO: Attacking units suffer DOUBLE CASUALTIES in the first round of combat
            //TO DO: The MAGINOT_LINE provides two FIRST STRIKE shots at 6
            }
        else if self.terrain == AIRBORNE_ASSAULT
            {
            army.changeUnitValue(Value: 1, Unit: AIRBORNE_INFANTRY, Attack: true)
            }
        }/*Calc.adjustArmyForTerrain()*/
    
    //************************************************************
    // Function Name: Calc.terrainModifierEnd()
    //
    // Details: This function reverts combat values to pre-terrain
    // modified values
    //************************************************************
    func terrainModifierEnd(Army army:Army)
        {
        if self.terrain == MOUNTAIN_TERRITORY || self.terrain == MOUNTAIN_BORDER
            {
            army.changeClassValue(Value: 1, UnitClass: "INFANTRY", Attack: true)
            army.changeClassValue(Value: 1, UnitClass: "ARTILLERY", Attack: true)
            army.changeClassValue(Value: 1, UnitClass: "VEHICLE", Attack: true)
            army.changeUnitValue(Value: -1, Unit: MOUNTAIN_INFANTRY, Attack: true)
            army.changeUnitValue(Value: -1, Unit: GURKHA, Attack: true)
            }
        else if self.terrain == DESERT_TERRITORY || self.terrain == DESERT_BORDER
            {
            army.changeClassValue(Value: 1, UnitClass: "INFANTRY", Attack: true)
            army.changeClassValue(Value: 1, UnitClass: "ARTILLERY", Attack: true)
            army.changeClassValue(Value: 1, UnitClass: "VEHICLE", Attack: true)
            army.changeUnitValue(Value: -1, Unit: FRENCH_FOREIGN_LEGION, Attack: true)
            //TO DO: Must utilize Army.retreatUnit() to make VEHICLE class units retreat on a roll of 12
            }
        else if self.terrain == JUNGLE_TERRITORY || self.terrain == JUNGLE_BORDER
            {
            army.changeClassValue(Value: 2, UnitClass: "VEHICLE", Attack: true)
            army.changeClassValue(Value: 2, UnitClass: "VEHICLE", Attack: false)
            if self.terrain == JUNGLE_TERRITORY
                {
                army.changeUnitValue(Value: 1, Unit: GURKHA, Attack: false)
                }
            //TO DO: JUNGLES also affect STRATEGIC_BOMBERS during carpet bombing
            }
        else if self.terrain == MARSH_TERRITORY || self.terrain == MARSH_BORDER
            {
            army.changeClassValue(Value: 2, UnitClass: "VEHICLE", Attack: true)
            army.changeClassValue(Value: 2, UnitClass: "VEHICLE", Attack: false)
            }
        else if self.terrain == RIVER
            {
            army.changeClassValue(Value: 1, UnitClass: "INFANTRY", Attack: true)
            army.changeClassValue(Value: 1, UnitClass: "VEHICLE", Attack: true)
            army.changeUnitValue(Value: -1, Unit: MARINE_INFANTRY, Attack: true)
            army.changeUnitValue(Value: -1, Unit: SPECIAL_NAVAL_LANDING_FORCE, Attack: true)
            army.changeUnitValue(Value: -1, Unit: UNITED_STATES_MARINE_CORPS, Attack: true)
            }
        else if self.terrain == CITY
            {
            army.changeClassValue(Value: -1, UnitClass: "INFANTRY", Attack: false)
            //TO DO: INFANTRY class units target select of 1 against VEHICLE class units
            }
        else if self.terrain == CITY_SURROUNDED
            {
            army.changeClassValue(Value: 1, UnitClass: "INFANTRY", Attack: false)
            army.changeClassValue(Value: 1, UnitClass: "ARTILLERY", Attack: false)
            army.changeClassValue(Value: 1, UnitClass: "VEHICLE", Attack: false)
            }
        else if self.terrain == FORTIFICATION
            {
            army.changeClassValue(Value: -2, UnitClass: "INFANTRY", Attack: false)
            army.changeClassValue(Value: -2, UnitClass: "ARTILLERY", Attack: false)
            army.changeClassValue(Value: -2, UnitClass: "VEHICLE", Attack: false)
            //TO DO: Fortifications provide two FIRST STRIKE shots at 5
            }
        else if self.terrain == MAGINOT_LINE
            {
            army.changeClassValue(Value: -3, UnitClass: "INFANTRY", Attack: false)
            army.changeClassValue(Value: -3, UnitClass: "ARTILLERY", Attack: false)
            army.changeClassValue(Value: -3, UnitClass: "VEHICLE", Attack: false)
            //TO DO: Attacking units suffer DOUBLE CASUALTIES in the first round of combat
            //TO DO: The MAGINOT_LINE provides two FIRST STRIKE shots at 6
            }
        else if self.terrain == AIRBORNE_ASSAULT
            {
            army.changeUnitValue(Value: -1, Unit: AIRBORNE_INFANTRY, Attack: true)
            }
        }/*Calc.terrainModifierEnd()*/
    
    
    
    
    //************************************************************
    // Function Name: Calc.runCalc()
    //
    // Details: This function runs the battle that is currently
    //      stored in the object 10000 times and prints the
    //      outcome.
    //
    //      TODO: We should have the outcome data stored in
    
    //
    //************************************************************
    func runCalc()
        {
        /*-----------------------------------------
        Calculate the Battle 10000 times.
        -----------------------------------------*/
        for i in 1...NUMB_RUNS
            {
            print("starting battle round \(i)...")
            
            /*-----------------------------------------
            Create temporary copies of our armies so
                we don't lose them in each battle
                iteration
            -----------------------------------------*/
            //print("Attack army size = ", attacker.getUnitCount() )
            var tempAtt:Army = .init(Attack: true)
            tempAtt.copy(Army: attacker)
            adjustArmyForTerrain(Army: tempAtt)
            var tempDef:Army = .init(Attack: false)
            tempDef.copy(Army: defender)
            adjustArmyForTerrain(Army: tempDef)
            dbg_print( Message: "Number of starting attack units\(tempAtt.hitCount)" )
            dbg_print( Message: "Number of starting defend units\(tempDef.hitCount)" )
            
            /*-----------------------------------------
            Run this battle iteration on the temporary
                army objects.
            -----------------------------------------*/
            var localRoundCount = 0
            while tempAtt.hitCount > 0 && tempDef.hitCount > 0
                {
                var defendHitCount = 0
                var attackHitCount = 0
                /*-----------------------------------------
                Check for terrain modification
                -----------------------------------------*/
                if localRoundCount == terrainDuration
                    {
                    terrainModifierEnd(Army: tempAtt)
                    terrainModifierEnd(Army: tempDef)
                    }
                if( ( self.terrain == DESERT_BORDER ) &&
                    ( localRoundCount == 1 ) )
                    {
                    vehicleRetreat = false
                    }
                
                /*-----------------------------------------
                If first round, roll first strike
                 -----------------------------------------*/
                if localRoundCount == 0
                    {
                    attackHitCount = rollFirstStrike(Army: tempAtt, Opposing_Army: tempDef, Attack: true)
                    defendHitCount = rollFirstStrike(Army: tempDef, Opposing_Army: tempAtt, Attack: false)
                    print("Attacker hits \(attackHitCount) first strike hits")
                    print("Defender hits \(defendHitCount) frist strike hits")
                    /*-----------------------------------------
                    Remove Attacker First Strike Hits
                    -----------------------------------------*/
                    dbg_print( Message: "attacker units remaining: \(tempAtt.hitCount)")
                    dbg_print( Message: "defender units remaining: \(tempDef.hitCount)")
                    if attackHitCount == 0
                        {
                        }
                    else if attackHitCount >= tempDef.hitCount
                        {
                        attackHitCount = tempDef.hitCount
                        dbg_print( Message: "hit count adjusted to: \(attackHitCount)")
                        tempDef = removeCasualties(Army: tempDef, numHits: attackHitCount)
                        }
                    else
                        {
                        tempDef = removeCasualties(Army: tempDef, numHits: attackHitCount)
                        }
                        
                    /*-----------------------------------------
                    Remove Defender First Strike Hits
                    -----------------------------------------*/
                    if defendHitCount == 0
                        {
                        }
                    else if defendHitCount >= tempAtt.hitCount
                        {
                        defendHitCount = tempAtt.hitCount
                        dbg_print( Message: "hit count adjusted to: \(defendHitCount)")
                        tempAtt = removeCasualties(Army: tempAtt, numHits: defendHitCount)
                        }
                    else
                        {
                        tempAtt = removeCasualties(Army: tempAtt, numHits: defendHitCount)
                        }
                    attackHitCount = 0
                    defendHitCount = 0
                    }
                /*-----------------------------------------
                Count hits for each side
                -----------------------------------------*/
                defendHitCount = countTotalHits(Army: tempDef, Opposing_Army: tempAtt, Round: localRoundCount )
                print("# of defender hits: \(defendHitCount)")
                attackHitCount = countTotalHits(Army: tempAtt, Opposing_Army: tempDef, Round: localRoundCount )
                print("# of attacker hits: \(attackHitCount)")
                dbg_print( Message: "attacker: \(attackHitCount)")
                dbg_print( Message: "defender: \(defendHitCount)")

                /*-----------------------------------------
                Remove Attacker Hits
                -----------------------------------------*/
                dbg_print( Message: "attacker units remaining: \(tempAtt.hitCount)")
                dbg_print( Message: "defender units remaining: \(tempDef.hitCount)")
                if attackHitCount == 0
                    {
                    }
                else if attackHitCount >= tempDef.hitCount
                    {
                    attackHitCount = tempDef.hitCount
                    dbg_print( Message: "hit count adjusted to: \(attackHitCount)")
                    tempDef = removeCasualties(Army: tempDef, numHits: attackHitCount)
                    }
                else
                    {
                    tempDef = removeCasualties(Army: tempDef, numHits: attackHitCount)
                    }
                    
                /*-----------------------------------------
                Remove Defender Hits
                -----------------------------------------*/
                if defendHitCount == 0
                    {
                    }
                else if defendHitCount >= tempAtt.hitCount
                    {
                    defendHitCount = tempAtt.hitCount
                    dbg_print( Message: "hit count adjusted to: \(defendHitCount)")
                    tempAtt = removeCasualties(Army: tempAtt, numHits: defendHitCount)
                    }
                else
                    {
                    tempAtt = removeCasualties(Army: tempAtt, numHits: defendHitCount)
                    }
                /*-----------------------------------------
                Check for Winner/Tie
                -----------------------------------------*/
                if tempAtt.hitCount == 0 && tempDef.hitCount == 0
                    {
                    print("it's a TIE!")
                    tieCount += 1
                    }
                else if tempAtt.hitCount == 0 && tempDef.hitCount > 0
                    {
                    print("DEFENSE wins")
                    defenderWins += 1
                    }
                else if tempDef.hitCount == 0 && tempAtt.hitCount > 0
                    {
                    print("ATTACK wins")
                    attackerWins += 1
                    }
                localRoundCount += 1
                //print("attacking unit count: \(tempAtt.unitCount)")
                //print("defending unit count: \(tempDef.unitCount)")
                } /* End while() loop */
                
            attackerCost = attackerCost + tempAtt.IPPsLost
            defenderCost = defenderCost + tempDef.IPPsLost
            } /* end for() loop */
            
        /*-----------------------------------------
        Print Results
        -----------------------------------------*/
        print("Attacker Win %: \(attackerWins/10)")
        print("Defender Win %: \(defenderWins/10)")
        print("Tie %: \(tieCount/10)")

        print("Attacker avg cost: \(attackerCost/1000)")
        print("Defender avg cost: \(defenderCost/1000)")

        } /* Calc.runCalc() */
        
} /* Class Calc */
