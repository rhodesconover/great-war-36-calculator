//
//  Army.swift
//  GW36practiceCalc
//
//  Created by Cade Dotson on 12/28/20.
//

import Foundation

class Army{
    
    var hitCount:Int = 0
    var IPPsLost:Float = 0
    
    var attack:Bool
    
    var Units = [[Unit]]()
    
    
    
    //************************************************************
    // Function Name: Army.init(Attack attack:Bool)
    //
    // Details: This constructor creates 52 empty lists to store
    //      units, it also sets the attack or defend value of the
    //      the army
    //************************************************************
    init(Attack attack:Bool)
        {
        self.attack = attack
        for _ in 0...( UNIT_TYPE_COUNT - 1 )
            {
            let temp = [Unit]()
            Units.append(temp)
            }
        } /* Army.init(Attack attack:Bool) */
    
    
    //************************************************************
    // Function Name: Army.getUnitCount()
    //
    // Details: This function simply returns the number of units
    //      in the battle. 
    //
    //************************************************************
    func getUnitCount () -> Int
        {
        return hitCount
        } /* Army.getUnitCount() */
    
    
    //************************************************************
    // Function Name: Army.addUnit (Unit unit:Int)
    //
    // Details: This function adds a unit of the given type to the
    //      army, it also updates Army.unitCount
    //
    //
    //************************************************************
    func addUnit (Unit unit:Int)
        {
        self.Units[unit].append(Unit(Name: unit))
        self.hitCount += 1
        if (unit == BATTLESHIP || unit == FAST_BATTLESHIP || unit == FLEET_CARRIER || unit == HEAVY_CARRIER)
            {
            self.hitCount += 1
            }
        if (unit == HEAVY_BATTLESHIP)
            {
            self.hitCount += 2
            }
        }   /* Army.addUnit(Unit unit:Int) */
    
    //************************************************************
    // Function Name: Army.remUnit(Unit unit:Int)
    //
    // Details: This function removes a unit of the given type from
    //      from the army, it also updates Army.unitCount
    //
    //
    //************************************************************
    func remUnit (Unit unit:Int)
        {
        if self.Units[unit][self.Units[unit].count-1].hits > 1
            {
            self.Units[unit][self.Units[unit].count-1].hits -= 1
            if (self.Units[unit][self.Units[unit].count-1].name == BATTLESHIP)
                {
                print("damaging battleship")
                self.Units[unit][self.Units[unit].count-1].attack -= 2
                self.Units[unit][self.Units[unit].count-1].defense -= 2
                self.hitCount -= 1
                }
            else if (self.Units[unit][self.Units[unit].count-1].name == FAST_BATTLESHIP)
                {
                self.Units[unit][self.Units[unit].count-1].attack -= 2
                self.Units[unit][self.Units[unit].count-1].defense -= 2
                self.hitCount -= 1
                }
            else if (self.Units[unit][self.Units[unit].count-1].name == HEAVY_BATTLESHIP)
                {
                self.Units[unit][self.Units[unit].count-1].attack -= 2
                self.Units[unit][self.Units[unit].count-1].defense -= 2
                self.hitCount -= 1
                }
            else if (self.Units[unit][self.Units[unit].count-1].name == FLEET_CARRIER)
                {
                self.Units[unit][self.Units[unit].count-1].defense -= 1
                self.hitCount -= 1
                }
            else if (self.Units[unit][self.Units[unit].count-1].name == HEAVY_CARRIER)
                {
                self.Units[unit][self.Units[unit].count-1].defense -= 1
                self.hitCount -= 1
                }
            }
        else
            {
                print("Removing a casualty")
                self.IPPsLost += Float(Units[unit][0].cost)
                self.Units[unit].remove(at: Units[unit].count-1)
                self.hitCount -= 1
            }
        }  /* Army.remUnit(Unit unit:Int) */
    
    //************************************************************
    // Function Name: Army.retreatUnit(Unit unit:Int)
    //
    // Details: This function removes a unit of the given type from
    //      from the army, it also updates Army.unitCount
    //
    //          DOES NOT update IPPsLost
    //************************************************************
    func retreatUnit (Unit unit:Int)
        {
        if self.Units[unit].count > 0
            {
            self.Units[unit].remove(at: Units[unit].count-1)
            self.hitCount -= 1
            }
        }  /* Army.remUnit(Unit unit:Int) */
    
    //************************************************************
    // Function Name: Army.copy()
    //
    // Details: This function takes an army object passed in by
    //      the caller and overwrites THIS object in memory with
    //      the data passed in.
    //
    // This function DOES NOT retain altered attack or defense values
    // in the resultant Army
    //************************************************************
    func copy(Army oldArmy:Army)
        {
        for i in 0...( UNIT_TYPE_COUNT - 1 )
            {
            if oldArmy.Units[i].count > 0
                {
                //print("Found units")
                for _ in 1...oldArmy.Units[i].count
                    {
                    //print("Adding a unit!")
                    self.addUnit(Unit: i)
                    }
                }
            }
        } /* Army.copy() */
        
        
    //************************************************************
    // Function Name: Army.printMe()
    //
    // Details: This function prints out all units present in an
    //      army quickly for debugging purposes.
    //
    //************************************************************
    func printMe()
        {
        if( self.attack == true )
            {
            print("\nThis is the attacking army:")
            }
        else
            {
            print("\nThis is the defending army:")
            }
        for i in 0...( UNIT_TYPE_COUNT - 1 )
            {
            if( i == DAMAGE_BATTLESHIPS ) || ( i == DAMAGE_CARRIERS )
                {
                continue
                }
            if( self.Units[ i ].count > 0 )
                {
                print( "There are ",self.Units[ i ].count," ",UNIVERSAL_UNIT_DATA[ i ][ UUSI ]," units." )
                }
            }
        }
    
    //************************************************************
    // Function Name: Army.changeClassValue()
    //
    // Details: This function changes the attack or defense values of all
    //units in the given unit class by the given integer value
    //(Can be positive or negative change)
    //************************************************************
    func changeClassValue(Value value:Int, UnitClass unitClass:String, Attack attack:Bool)
        {
        for i in 0...51
            {
            if self.Units[i].count > 0
                {
                if self.Units[i][0].unitClass == unitClass
                    {
                    for j in 0..<self.Units[i].count
                        {
                        if attack
                            {
                            self.Units[i][j].attack += value
                            }
                        else
                            {
                            self.Units[i][j].defense += value
                            }
                        }
                    }
                }
            }
        }/*Army.changeClassValue()*/
    
    
    //************************************************************
    // Function Name: Army.changeUnitValue()
    //
    // Details: This function changes the attack or defense values of all
    //units in the given unit class by the given integer value
    //(Can be positive or negative change)
    //************************************************************
    func changeUnitValue(Value value:Int, Unit unit:Int, Attack attack:Bool)
        {
        if self.Units[unit].count > 0
            {
            for i in 0..<self.Units[unit].count
                {
                if attack
                    {
                    self.Units[unit][i].attack += value
                    }
                else
                    {
                    self.Units[unit][i].defense += value
                    }
                }
            }
        }/*Army.changeUnitValue()*/
    
    
} /* class Army */
 
