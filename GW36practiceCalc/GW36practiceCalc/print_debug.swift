

import Foundation


let DBG_MODE = false


func dbg_print( Message msg: String )
    {
    if DBG_MODE
        {
        print( msg )
        }
    } /* dbg_print() */
